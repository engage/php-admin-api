<?php
namespace app\admin\controller;
use app\admin\model\AdminRank as Mod;
use app\admin\model\AdminRankFunc;
use app\admin\model\AdminUser as AdminUserModel;
use yichenthink\utils\ReturnMsg;

class AdminRank extends Base {

	// 管理员权限 查询所有职务权限列表
	public function list($p = 0, $trashed = null) {
		$num = 100;
		$code = 400;
		$message = '';
		$startNum = $num * $p;
		if ($trashed) {
			$data = Mod::onlyTrashed()
				->with('AdminRankFunc.Adminfunc')
				->visible(['id', 'rank_name', 'create_time', 'update_time', 'admin_rank_func'])
				->limit($startNum, $num)
				->select();
		} else {
			$data = Mod::with('AdminRankFunc.Adminfunc')
				->visible(['id', 'rank_name', 'create_time', 'update_time', 'admin_rank_func'])
				->limit($startNum, $num)
				->select();
		}
		if ($data != Null) {
			$code = 200;
		}
		ReturnMsg::returnMsg($code, $message, $data);
	}
	// 根据当前登陆的用户id 查询自己所拥有的权限和功能
	public function myRankFunc() {
		$code = 400;
		$message = '';
		$userInfo = $this->userInfo;
		$map = [];
		$map[] = ['user_id', '=', $userInfo['uid']];
		$data = AdminUserModel::where($map)
			->with(['AdminRankFunc.Adminfunc', 'AdminRank'])
			->visible(['id', 'rank_name', 'user_name', 'create_time', 'update_time', 'admin_rank_func'])
			->find();

		if ($data != Null) {
			$code = 200;
		}
		ReturnMsg::returnMsg($code, $message, $data);
	}
	// 根据传入的用户id 查询用户的权限和功能
	public function UserRankFunc($user_id = 0) {
		$code = 400;
		$message = '';
		$admin = $this->admin;
		$map = [];
		$map[] = ['admin_id', '=', $admin['admin_id']];
		$map[] = ['user_id', '=', $user_id];
		$data = AdminUserModel::where($map)
			->with(['AdminRankFunc.Adminfunc', 'AdminRank'])
			->visible(['id', 'rank_name', 'user_name', 'create_time', 'update_time', 'admin_rank_func'])
			->find();

		if ($data != Null) {
			$code = 200;
		}
		ReturnMsg::returnMsg($code, $message, $data);
	}
	// 增加一条新职务数据
	public function add($rank_name) {
		//设置允许修改的字段
		$message = '';
		$code = 400;
		$map = [];
		$map[] = ['admin_id', '=', $this->userInfo['uid']];
		$map[] = ['rank_name', '=', $rank_name];
		$R = Mod::where($map)->find();
		if (Null == $R) {
			$Mod = new Mod;
			$Mod->admin_id = $this->userInfo['uid'];
			$Mod->rank_name = $rank_name;
			$Mod->save();
			$map[] = ['id', '=', $Mod->id];
			if (false !== $R) {
				$code = 200;
				$data = $Mod::withTrashed()
					->where($map)
					->visible(['id', 'rank_name', 'create_time', 'update_time'])
					->find();
				ReturnMsg::returnMsg($code, $message, $data);
			}

		} else {

			$message = '职务已存在';
		}
		ReturnMsg::returnMsg($code, $message);
	}
	// 删除一条职务
	public function delete($id = 0, $isTrue = false) {
		$map = [];
		$map[] = ['id', '=', $id];
		$Mod = Mod::withTrashed()->where($map)
			->find();
		if ($Mod) {
			if ($isTrue) {
				AdminRankFunc::where('rank_id', $id)->delete();
			}
			$Mod->delete($isTrue);
		}
		if (0 !== $Mod) {
			$code = 200;
		} else {
			$code = 400;
		}

		ReturnMsg::returnMsg($code, '', $Mod);
	}

	//恢复一条职务
	public function restore($id = 0) {

		$map = [];
		$map[] = ['id', '=', $id];
		$code = 400;
		$Mod = Mod::onlyTrashed()
			->where($map)
			->find();
		# code...
		if ($Mod) {
			$code = 200;
			$Mod->restore();
		}
		// $user = UserModel::restore($list);
		ReturnMsg::returnMsg($code);
	}
	// 修改职务名称
	public function update($id, $rank_name) {

		//设置允许修改的字段
		$message = '';

		$map = [];
		$map[] = ['id', '=', $id];

		$code = 400;
		$Mod = Mod::where($map)->find();

		if (null != $Mod) {
			$Mod->rank_name = $rank_name;
			$R = $Mod->save();
			if (false !== $R) {

				$code = 200;
			} else {
				$message = '操作失败';
				$code = 400;
			}
		} else {
			$message = '职位不存在';
			$code = 400;
		}
		ReturnMsg::returnMsg($code, $message);
	}

}
