<?php
namespace app\admin\validate;

use think\Validate;

class News extends Validate {
	protected $rule = [
		'title|标题' => 'min:5|max:32',
		'content|内容' => 'max:1000|min:10',
		'state|状态' => 'number|between:0,2',
		'category_id|类型' => 'number',
		'image' => 'min:1',
		// 'image|图片' => 'min:2048|max:4088000|regex:/^data:((\w+)\/(\w+));base64,/',
	];
	protected $message = [
		// 'image.regex' => '图片格式有误',
		// 'image.max' => '图片尺寸太大',
		// 'image.min' => '图片尺寸太小',
		'category_id' => '未选择分类',
		'title.min' => '标题最少需要5个字符',
		'title.max' => '标题最多不能超过32个字符',
		'state.number' => '状态需要数字',
		'state.between' => '状态只能在0-1之间',
		'content.min' => '内容最少需要10个字符',
		'content.max' => '内容最多不能超过1000个字符',
	];

	// edit 验证场景定义
	public function sceneAdd() {
		return $this->append('title', 'require')
			->append('content', 'require')
			->append('image', 'require')
			->append('category_id', 'require')
			->append('state', 'require')
			->append('category_id', 'require');
	}
	// edit 验证场景定义
	public function sceneImage() {
		return $this->append('title', 'require')
			->append('content', 'require')
			->append('image', 'require');
	}
}
?>