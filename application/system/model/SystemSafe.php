<?php
namespace app\system\model;

class SystemSafe extends \app\common\model\SystemSafe {

	// 关联分组模型
	public function systemSafeGrade() {
		return $this->hasOne('systemSafeGrade', 'id', 'grade_id')->bind([
			'grade_name' => 'name',
			'grade_content' => 'content',
		]);
	}
	public function User() {
		return $this->hasOne('User', 'id', 'user_id')->bind([
			"username" => "username",
			"password" => "password",
			"user_state" => "state",
			"user_group" => "group",
			"user_ip" => "ip",
		]);
	}

}