<?php
namespace app\system\model;

class AdminGroupFunc extends \app\common\model\AdminGroupFunc {

	public function admin() {
		return $this->hasOne('admin', 'rank_id', 'rank_id');
	}
	public function adminFunc() {
		return $this->hasOne('admin_func', 'id', 'func_id')->bind([
			'action' => 'action',
			'func_name' => 'func_name',
			'method' => 'method',
			'access' => 'access',
			'ban_param' => 'ban_param',
			'only_param' => 'only_param',
		]);
	}

	// 批量新增 和批量删除暂时废弃
	public function add($group_id, $func_id_arr = []) {
		if (is_array($func_id_arr) && count($func_id_arr)) {
			$list = [];
			foreach ($func_id_arr as $k => $v) {
				$list[] = ['group_id' => $group_id, 'func_id' => $v];
			}
			return $this->saveAll($list);
		}
	}
	// 批量新增 和批量删除暂时废弃
	public function del($group_id, $func_id_arr = []) {
		if (is_array($func_id_arr) && count($func_id_arr)) {
			$list = [];
			foreach ($func_id_arr as $k => $v) {
				$list[] = $v;
			}
			return $this->where([['group_id', '=', $group_id], ['func_id', 'in', $list]])->delete();
		}
	}
}