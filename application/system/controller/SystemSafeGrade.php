<?php
namespace app\system\controller;
use app\system\model\SystemSafeGrade as Mod;
use app\system\model\User as UserModel;
use yichenthink\utils\ReturnMsg;

class SystemSafeGrade extends AdminBase {

	public function list() {
		// 查询未删除的
		$data = Mod::select();
		ReturnMsg::returnMsg(200, '', $data);
	}

}
