<?php
namespace app\system\controller;
use app\system\model\Admin as AdminModel;
use app\system\model\AdminFunc as Mod;
use think\facade\Request;
use yichenthink\utils\ReturnMsg;

class AdminFunc extends Base {
	// 管理员显示所有功能名称列表
	//如果不是管理员只显示成员所拥有的功能名称
	public function list($p = 0, $access = 'system', $trashed = null) {
		// 查询被删除的
		if ($trashed) {
			$data = Mod::onlyTrashed()
				->where('access', $access)
				->select()
				->visible(['id', 'func_name', 'action']);
		} else {
			$data = Mod::where('access', $access)->select()
				->visible(['id', 'func_name', 'action']);
		}

		ReturnMsg::returnMsg(200, '', $data);
	}
	// 显示所有系统功能详细列表
	public function detailedList($p = 0, $access = 'system', $trashed = '0') {
		$num = 100;
		// 查找页数
		$startNum = $num * $p;
		$message = '';
		$data = [];

		$Mod = new Mod;
		// 查询被删除的
		if ($trashed) {
			$data = $Mod::onlyTrashed()
				->where('access', $access)
				->visible([
					'id',
					'func_name',
					'methods',
					'access',
					'action',
					'explain',
					'method',
					'recaption',
					'ban_param',
					'only_param',
					'update_time',
					'create_time',
					'user_id',
					'update_user_id',
				])
				->limit($startNum, $num)
				->select();
		} else {
			$data = $Mod::where('access', $access)->limit($startNum, $num)
				->visible([
					'id',
					'func_name',
					'methods',
					'access',
					'action',
					'explain',
					'method',
					'recaption',
					'ban_param',
					'only_param',
					'update_time',
					'create_time',
					'user_id',
					'update_user_id',
				])
				->select();
		}

		ReturnMsg::returnMsg(200, $message, $data);
	}

// 给系统增加一个新功能api接口
	public function add() {
		//设置允许修改的字段
		$message = '';
		$code = 400;
		$map = [];
		$map[] = ['user_id', '=', $this->userInfo['uid']];
		// 过滤post数组中的非数据表字段数据
		$data = Request::only([
			'func_name',
			'access',
			'action',
			'explain',
			'method',
			'recaption',
			'ban_param',
			'only_param',
		]);
		isset($data['method']) && $data['method'] = strtoupper($data['method']); //转大写
		$data['user_id'] = $this->userInfo['uid'];
		$Mod = new Mod;
		foreach ($data as $key => $value) {
			$Mod->$key = $value;
		}

		if ($Mod->save()) {
			$code = 200;

		}
		ReturnMsg::returnMsg($code, $message, $Mod);
	}
// 给系统增加一个新功能api接口
	public function update($id) {
		//设置允许修改的字段
		$message = '';
		$code = 400;
		$map = [];
		$map[] = ['user_id', '=', $this->userInfo['uid']];
		// 过滤post数组中的非数据表字段数据
		$data = Request::only([
			'func_name',
			'access',
			'action',
			'explain',
			'method',
			'recaption',
			'ban_param',
			'only_param',
		]);
		isset($data['method']) && $data['method'] = strtoupper($data['method']); //转大写
		$data['user_id'] = $this->userInfo['uid'];
		$Mod = new Mod;

		if ($Mod->save($data, ['id' => $id])) {
			$code = 200;

		}
		ReturnMsg::returnMsg($code, $message, $Mod);
	}
	//恢复一条
	public function restore($id = 0) {
		$Mod = Mod::onlyTrashed()->find($id);
		# code...
		if ($Mod) {
			# code...
			$Mod->restore();
		}
		ReturnMsg::returnMsg($code = 200, '成功恢复了1条数据');
	}
	// 删除一条
	public function delete($id = 0, $isTrue = false) {

		$user = Mod::withTrashed()->where('id', $id)
			->find();
		$user->delete($isTrue);
		if (0 !== $user) {
			$code = 200;
		} else {
			$code = 400;
		}

		ReturnMsg::returnMsg($code, '', $user);
	}
}
