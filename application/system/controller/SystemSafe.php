<?php
namespace app\system\controller;
use app\system\model\SystemSafe as Mod;
use app\system\model\User as UserModel;
use yichenthink\utils\ReturnMsg;

class SystemSafe extends AdminBase {

	public function list($trashed = null) {
		$message = '';
		$code = 400;
		// 查询被删除的
		if ($trashed) {
			$data = Mod::onlyTrashed()
				->select()
				->visible(['id', 'name', 'create_time', 'update_time', 'cache_time', 'grade_id', 'user_id', 'grade_content']);
		} else {
			// 查询未删除的
			$data = Mod::with(['systemSafeGrade'])
				->select()
				->visible(['id', 'safe_name', 'create_time', 'update_time', 'cache_time', 'grade_id', 'user_id', 'grade_content']);
		}
		if ($data != Null) {
			$code = 200;
			$message = '成功';
		} else {
			$message = '没找到数据';
		}
		ReturnMsg::returnMsg($code, $message, $data);
	}
	public function detail($trashed = null) {
		$message = '';
		$code = 400;
		// 查询被删除的
		if ($trashed) {
			$data = Mod::onlyTrashed()
				->find()
				->visible(['id', 'name', 'create_time', 'update_time', 'cache_time', 'grade_id', 'user_id', 'grade_content']);
		} else {
			// 查询未删除的
			$data = Mod::with(['systemSafeGrade'])
				->find()
				->visible(['id', 'safe_name', 'create_time', 'update_time', 'cache_time', 'grade_id', 'user_id', 'grade_content']);
		}
		if ($data != Null) {
			$code = 200;
			$message = '成功';
		} else {
			$message = '没找到数据';
		}
		ReturnMsg::returnMsg($code, $message, $data);
	}

	// 修改
	public function update($id, $form = []) {

		// ReturnMsg::returnMsg($code = 400, $message = '成功', $form);
		//设置允许修改的字段
		$upfield = ['grade_id' => 'grade_id', 'user_id' => 'user_id', 'cache_time' => 'cache_time'];
		$message = '';
		$Mod = Mod::where('id', $id)->find();

		if (null != $Mod) {
			foreach ($upfield as $key => $value) {
				# code...
				if (isset($form[$key]) && $form[$key] != $Mod[$value]) {
					# code...
					$Mod->$value = $form[$value];
				}
			}
			$R = $Mod->save();
			if (false !== $R) {

				$code = 200;
			} else {
				$message = '操作失败';
				$code = 400;
			}
		} else {
			$message = '用户不存在';
			$code = 400;
		}
		ReturnMsg::returnMsg($code, $message, $Mod);
	}

}
