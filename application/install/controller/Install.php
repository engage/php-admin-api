<?php
namespace app\install\controller;
use app\common\model\SystemSafe;
use app\common\model\User;
use app\common\myconfig\Config as myConfig;
use think\Controller;
use think\Db;
use think\facade\Config;
use think\facade\Request;
use yichenthink\utils\Encrypt;
use yichenthink\utils\ReturnMsg;
use yichenthink\utils\SqlHandle;

/**
 *
 */
class Install extends controller {
	public $sqlTable = ''; //数据表文件.sql
	public $module = [];
	// 默认数据库配置选项
	public $db_config = [
		// 数据库类型
		'type' => 'mysql',
		// 服务器地址
		'hostname' => '127.0.0.1',
		// 数据库名
		'database' => 'admin',
		// 用户名
		'username' => 'root',
		// 密码
		'password' => 'root',
		// 端口
		'hostport' => '',
		// 连接dsn
		'dsn' => '',
		// 数据库连接参数
		'params' => [],
		// 数据库编码默认采用utf8
		'charset' => 'utf8',
		// 数据库表前缀
		'prefix' => '',
	];
	public $dbConnect = 'config'; //默认数据库连接
	protected function initialize() {
		$param = Request::param();
		$this->module = myConfig::$module;
		// 根据模型配置相应的数据库
		if (isset($param['model']) && isset($this->module[$param['model']])) {
			$this->dbConnect = $this->module[$param['model']]['db_config'];
			$this->sqlTable = $this->module[$param['model']]['dbTable'];
		} else {
			ReturnMsg::returnMsg(400, '不存在的模型', Request::param());
		}

	}
	// 根据模型名获取Db连接
	public function getDbModel($name = 'User') {
		$configObj = myConfig::$db_connection;
		if (isset($configObj[$name])) {
			try {
				return Db::connect($configObj[$name]);
			} catch (\Exception $e) {
				ReturnMsg::returnMsg(400, '数据库连接失败,请检查' . $name . '模型是否已配置数据库');
			}

		} else {
			ReturnMsg::returnMsg(400, $name . '模型是否已配置数据库');
		}
	}

	/**
	 * 添加数据
	 */
	public function addDb() {
		$config = Config::get()['database'];
		$db_config = isset($config[$this->dbConnect]) ? $config[$this->dbConnect] : $this->db_config;
		$prefix = isset($db_config['prefix']) ? $db_config['prefix'] : '';
		// 如果存在前缀先把所有字段前缀替换进去

		$myDb = (NEW SqlHandle)->get_sql_array($this->sqlTable, $prefix);
		//执行sql语句
		$db = Db::connect($db_config);
		// 去掉外键约束防止删除不了
		$db->execute("SET FOREIGN_KEY_CHECKS=0");
		$num = 0;
		foreach ($myDb as $value) {
			if (strpos($value, 'INSERT') !== false) {
				// 如果存在表先删除
				try {
					$num++;
					$db->query(strstr($value, 'INSERT'));
				} catch (Exception $e) {
					$num--;
				}

			}
		}
		ReturnMsg::returnMsg(200, '添加了' . $num . '个数据表的内容');
	}
	/**
	 * 配置数据库
	 */
	public function setDbConfig($model = '') {
		// echo "<pre>";
		// print_r(Request::module());
		// return;
		$data = Request::param();
		$config = Config::get()['database'];
		$db_config = isset($config[$this->dbConnect]) ? $config[$this->dbConnect] : $this->db_config;
		// ReturnMsg::returnMsg(200, '修改成功' . $model, $db_config);
		// 下面只是示例，随便怎么操作$config['hello']
		foreach ($data as $key => $value) {
			if (isset($db_config[$key])) {
				$db_config[$key] = $value;
			}
		}
		$config[$this->dbConnect] = $db_config;
		$dir = '../application/config';
		$file = $dir . '/database.php';
		// print_r($config);
		// 目录如果不存在创建目录
		is_dir($dir) or mkdir($dir, 0777, true);
		$output = '<?php return ' . var_export($config, true) . ';'; //转成字符串拼接成php文件
		file_put_contents($file, $output); // 省略号处为原来的文件名
		ReturnMsg::returnMsg(200, '修改成功', $db_config);
	}
	/**
	 * 删除数据表
	 */
	public function delTable() {
		$database = config()['database'];
		$db_config = isset($database[$this->dbConnect]) ? $database[$this->dbConnect] : $this->db_config;
		$prefix = isset($db_config['prefix']) ? $db_config['prefix'] : '';
		// 如果存在前缀先把所有字段前缀替换进去

		$myDb = (NEW SqlHandle)->get_sql_array($this->sqlTable, $prefix);
		// print_r($myDb);
		//执行sql语句
		// 去掉外键约束防止删除不了
		$db = Db::connect($db_config);
		$db->execute("SET FOREIGN_KEY_CHECKS=0");
		foreach ($myDb as $value) {
			if (strpos($value, 'DROP') !== false) {
				// 如果存在删除
				try {
					$db->query(strstr($value, 'DROP'));
				} catch (Exception $e) {

				}

			}
		}
		$table = $db->query("show tables");
		ReturnMsg::returnMsg(200, '成功删除' . count($table) . '张数据表' . $db_config['prefix'], $table);
	}
	/**
	 * 创建数据表
	 */
	public function makeTable() {
		$database = config()['database'];
		$db_config = isset($database[$this->dbConnect]) ? $database[$this->dbConnect] : $this->db_config;
		$prefix = isset($db_config['prefix']) ? $db_config['prefix'] : '';
		// 如果存在前缀先把所有字段前缀替换进去
		$myDb = (new SqlHandle)->get_sql_array($this->sqlTable, $prefix);
		//执行sql语句
		$db = Db::connect($db_config);
		// 去掉外键约束防止删除不了
		$db->execute("SET FOREIGN_KEY_CHECKS=0");
		foreach ($myDb as $value) {
			if (strpos($value, 'DROP') !== false) {
				// 如果存在表先删除
				try {
					$db->query(strstr($value, 'DROP'));
				} catch (\Exception $e) {

				}

			} elseif (strpos($value, 'CREATE')) {
				// 创建
				$db->query(strstr($value, 'CREATE'));
			}
		}

		$table = $db->query("show tables");
		ReturnMsg::returnMsg(200, '成功创建' . count($table) . '张数据表', $table);
	}
	/**
	 * 新建台管理账号用户名密码
	 */
	public function makeAccount() {
		$param = Request::param();

		$code = 400;
		$msg = '';
		$map = [];
		try {
			$SystemSafe = SystemSafe::find();
		} catch (\Exception $e) {
			ReturnMsg::returnMsg($code, '请在配置基础数据库后进行操作', $e);
		}
		// Encrypt::make()两次md5加密
		$password = Encrypt::make($param['username'] . $param['password']);
		$old = null;
		if (!empty($SystemSafe) && isset($SystemSafe['user_id'])) {

			$old = $this->getDbModel('User')->name('user')->where('id', $SystemSafe['user_id'])->find();
			if (empty($old)) {

				$data = ['username' => $param['username'], 'password' => $password, 'id' => $SystemSafe['user_id']];
				$msg = "创建用户成功";
				$old2 = $this->getDbModel('User')->name('user')->where('username', $param['username'])->find();
				if ($old2) {

					ReturnMsg::returnMsg($code, '用户名已存在且非管理员,请更换新用户名', $old);
				}
				$this->getDbModel('User')->name('user')->insert($data);
			} else {
				$msg = '用户已存在,';
				$data = ['id' => $old['id'], 'create_time' => time(), 'update_time' => time()];
				if (isset($param['username']) && $param['username'] != $old['username']) {
					$msg .= '用户名被重置,';
					$data['username'] = $param['username'];
					if ($this->getDbModel('User')->name('user')->where('username', $param['username'])->find()) {
						ReturnMsg::returnMsg($code, '用户名已存在且非管理员,请更换新用户名', $old);
					}
				}
				if ($password != $old['password']) {
					$data['password'] = $password;
					$msg .= '密码被重置,';
				}
				if ($this->getDbModel('User')->name('user')
					->update($data)) {
					$code = 200;
				}
			}

		} else {

			$old = $this->getDbModel('User')->name('user')->where('username', $param['username'])->find();
			if (!empty($old)) {
				ReturnMsg::returnMsg($code, '用户已存在,请更换用户名', $old);
			}

			$data = ['username' => $param['username'], 'password' => $password, 'id' => User::makeId(), 'create_time' => time(), 'update_time' => time()];
			$msg = "创建用户";
			// $r = $user->save($data);

			$this->getDbModel('User')->name('user')->insert($data);
			$SystemData = [];
			$SystemSafe = new SystemSafe;
			$SystemData['safe_name'] = "超级管理员";
			$SystemData['user_id'] = $data['id'];
			$SystemSafe->save($SystemData);
			$code = 200;
		}

		// echo "installSql";
		ReturnMsg::returnMsg($code, $msg, $data);
	}
	// protected function initialize() {
	// 	$param = Request::param();
	// 	// 根据模型配置相应的数据库
	// 	if (isset($param['model']) && isset($this->module[$param['model']])) {
	// 		$this->dbConnect = $this->module[$param['model']]['db_config'];
	// 		$this->sqlTable = $this->module[$param['model']]['dbTable'];
	// 	} else {
	// 		ReturnMsg::returnMsg(400, '不存在的模型', Request::param());
	// 	}

	// }
	// /**
	//  * 创建数据表
	//  */
	// public function index() {
	// 	$data = Request::param();

	// 	$database = config()['database'];
	// 	$db_config1 = isset($database[$this->dbConnect]) ? $database[$this->dbConnect] : $this->config;
	// 	$prefix = isset($db_config1['prefix']) ? $db_config1['prefix'] : '';
	// 	// 如果存在前缀先把所有字段前缀替换进去
	// 	//读取文件内容

	// 	$myDb = (NEW SqlHandle)->get_sql_array($sqlFile = $this->sqlTable, $prefix);
	// 	//执行sql语句
	// 	// 去掉外键约束防止写入不了
	// 	$db = Db::connect($db_config1);
	// 	$db::execute("SET FOREIGN_KEY_CHECKS=0");
	// 	$count = 0;
	// 	foreach ($myDb as $value) {
	// 		if (strpos($value, 'INSERT') !== false) {
	// 			try {
	// 				$n = $db->execute(strstr($value, 'INSERT'));
	// 				$count = $count + $n;
	// 			} catch (Exception $e) {

	// 			}
	// 		}

	// 	}
	// 	ReturnMsg::returnMsg(200, '成功写入' . $count . '条数据', '');
	// }
	// /**
	//  * 添加数据
	//  */
	// public function addDb() {
	// 	$database = config()['database'];
	// 	$db_config1 = isset($database[$this->dbConnect]) ? $database[$this->dbConnect] : $this->config;
	// 	$prefix = isset($db_config1['prefix']) ? $db_config1['prefix'] : '';
	// 	// 如果存在前缀先把所有字段前缀替换进去

	// 	$myDb = (NEW SqlHandle)->get_sql_array($this->sqlTable, $prefix);
	// 	//执行sql语句
	// 	$db = Db::connect($db_config1);

	// 	// 去掉外键约束防止删除不了
	// 	$db->execute("SET FOREIGN_KEY_CHECKS=0");

	// 	$num = 0;
	// 	foreach ($myDb as $value) {
	// 		if (strpos($value, 'INSERT') !== false) {
	// 			// 如果存在表先删除
	// 			try {
	// 				$num++;
	// 				$db->query(strstr($value, 'INSERT'));
	// 			} catch (Exception $e) {
	// 				$num--;
	// 			}

	// 		}
	// 	}
	// 	ReturnMsg::returnMsg(200, '添加了' . $num . '个数据表的内容');
	// }
	// /**
	//  * 配置数据库
	//  */
	// public function setDbConfig() {
	// 	$data = Request::param();
	// 	$config = Config::get()['database'];
	// 	$config1 = isset($config[$this->dbConnect]) ? $config[$this->dbConnect] : $this->config;

	// 	// 下面只是示例，随便怎么操作$config['hello']
	// 	foreach ($data as $key => $value) {
	// 		$config1[$key] = $value;
	// 	}
	// 	$config[$this->dbConnect] = $config1;
	// 	$output = '<?php return ' . var_export($config, true) . ';'; //转成字符串拼接成php文件
	// 	file_put_contents('../application/config/database.php', $output); // 省略号处为原来的文件名
	// 	ReturnMsg::returnMsg(200, '修改成功', $config1);
	// }
	// /**
	//  * 删除数据表
	//  */
	// public function delTable() {
	// 	$database = config()['database'];
	// 	$db_config1 = isset($database[$this->dbConnect]) ? $database[$this->dbConnect] : $this->config;
	// 	$prefix = isset($db_config1['prefix']) ? $db_config1['prefix'] : '';
	// 	// 如果存在前缀先把所有字段前缀替换进去

	// 	$myDb = (NEW SqlHandle)->get_sql_array($this->sqlTable, $prefix);

	// 	//执行sql语句
	// 	// 去掉外键约束防止删除不了
	// 	$db = Db::connect($db_config1);
	// 	$db->execute("SET FOREIGN_KEY_CHECKS=0");
	// 	foreach ($myDb as $value) {
	// 		if (strpos($value, 'DROP') !== false) {
	// 			// 如果存在删除
	// 			try {
	// 				$db->query(strstr($value, 'DROP'));
	// 			} catch (Exception $e) {

	// 			}

	// 		}
	// 	}
	// 	$table = $db->query("show tables");
	// 	ReturnMsg::returnMsg(200, '成功删除' . count($table) . '张数据表', $table);
	// }
	// /**
	//  * 创建数据表
	//  */
	// public function makeTable() {
	// 	$database = config()['database'];
	// 	$db_config1 = isset($database[$this->dbConnect]) ? $database[$this->dbConnect] : $this->config;
	// 	$prefix = isset($db_config1['prefix']) ? $db_config1['prefix'] : '';
	// 	// 如果存在前缀先把所有字段前缀替换进去
	// 	$myDb = (new SqlHandle)->get_sql_array($this->sqlTable, $prefix);
	// 	// ReturnMsg::returnMsg(400, '不存在的模型', $myDb);
	// 	//执行sql语句
	// 	$db = Db::connect($db_config1);
	// 	// 去掉外键约束防止删除不了
	// 	$db->execute("SET FOREIGN_KEY_CHECKS=0");
	// 	foreach ($myDb as $value) {
	// 		if (strpos($value, 'DROP') !== false) {
	// 			// 如果存在表先删除
	// 			try {
	// 				$db->query(strstr($value, 'DROP'));
	// 			} catch (Exception $e) {

	// 			}

	// 		} elseif (strpos($value, 'CREATE') !== false) {
	// 			// 创建
	// 			$db->query(strstr($value, 'CREATE'));
	// 		}
	// 	}

	// 	$table = $db->query("show tables");
	// 	ReturnMsg::returnMsg(200, '成功创建' . count($table) . '张数据表', $table);
	// }
	// /**
	//  * 新建台管理账号用户名密码
	//  */
	// public function makeAccount() {
	// 	$param = Request::param();
	// 	$user = new User;
	// 	$code = 400;
	// 	$map = ['username' => $param['username']];
	// 	$old = $user->where($map)->find();
	// 	// Encrypt::make()两次md5加密
	// 	$data = ['username' => $param['username'], 'password' => Encrypt::make($param['username'] . $param['password']), 'id' => User::makeId()];
	// 	if ($old != Null) {
	// 		//超级管理员如果存在就删除
	// 		$SystemSafe = SystemSafe::where('user_id', $old['id'])->delete(true);
	// 		$old->delete(true);
	// 	};
	// 	$r = $user->save($data);
	// 	if ($r) {
	// 		$code = 200;
	// 		$SystemSafe = new SystemSafe;
	// 		$SystemSafe->safe_name = "超级管理员";
	// 		$SystemSafe->user_id = $user['id'];
	// 		$SystemSafe->save();
	// 		# code...
	// 	};
	// 	// echo "installSql";
	// 	ReturnMsg::returnMsg($code, '', $user);
	// }
}
?>