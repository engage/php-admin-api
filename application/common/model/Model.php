<?php
namespace app\common\model;
use app\common\myconfig\Config as myConfig;
use think\facade\Config;
use yichenthink\utils\ReturnMsg;

class Model extends \think\Model {
	protected $db_connection = [];
	// 直接使用配置参数名
	protected function initialize() {
		parent::initialize();
		$this->db_connection = myConfig::$db_connection;
		$this->initDbconnection();

	}
	// 查找模型是否有自定义的数据库配置 有就连自定义的库
	protected function initDbconnection() {
		if (!isset($this->connection) || empty($this->connection)) {
			$config = Config::get()['database'];
			// 用斜杠分割成数组
			$arr = explode('\\', get_class($this));
			$count = count($arr) - 1;

			// 从最右边依次截取到 模块名+模型名
			if (isset($arr[$count])) {

				if (isset($this->db_connection[$arr[$count]])) {
					$this->connection = $this->db_connection[$arr[$count]];
				}

				// 	$MOD = strtolower(preg_replace('/((?<=[a-z])(?=[A-Z]))/', '_', $arr[3])); //大写字母替换小写，并且大写字母前加下划线 匹配出当前被new 的对象的应用目录+模块名+模型目录名+模型类名
				// 	$MODarr = explode('_', $MOD);
				// 	$MODcount = count($MODarr);

				// 	// 判断根据模块名模型名，去数据库配置里查看是否有单独的数据库配置。如若有就使用该配置
				// 	if (isset($config['db_' . $arr[1] . '_' . $MOD])) {
				// 		// 判断模块下表全名 是否配单独配置数据库 有就匹配
				// 		$this->connection = $config['db_' . $arr[1] . '_' . $MOD];
				// 	} elseif ($MODcount > 1 && isset($config['db_' . $arr[1] . '_' . $MODarr[0] . '_' . $MODarr[1]])) {
				// 		//判断模块下 分表是否单独配置 有就匹配
				// 		$this->connection = $config['db_' . $arr[1] . '_' . $MODarr[0] . '_' . $MODarr[1]];
				// 	} elseif ($MODcount > 1 && isset($config['db_' . $arr[1] . '_' . $MODarr[0]])) {
				// 		//判断模块下 主表是否单独配置 有就匹配
				// 		$this->connection = $config['db_' . $arr[1] . '_' . $MODarr[0]];
				// 	} elseif (isset($config['db_' . $MOD])) {
				// 		// 判断模型下 表全名 是否配单独配置数据库 有就匹配

				// 		$this->connection = $config['db_' . $MOD];
				// 	} elseif ($MODcount > 1 && isset($config['db_' . $MODarr[0] . '_' . $MODarr[1]])) {
				// 		// 判断模型下 分表前是否单独配置 有就匹配
				// 		$this->connection = $config['db_' . $MODarr[0] . '_' . $MODarr[1]];
				// 	} elseif ($MODcount > 1 && isset($config['db_' . $MODarr[0]])) {
				// 		// 判断模型下 主表是否单独配置 有就匹配
				// 		$this->connection = $config['db_' . $MODarr[0]];
				// 	}

			}
		}
	}
}