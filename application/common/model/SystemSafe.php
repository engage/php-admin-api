<?php
namespace app\common\model;
use think\model\concern\SoftDelete;

class SystemSafe extends Model {

	use SoftDelete;
	protected $deleteTime = 'delete_time'; //软删除字段
	protected $defaultSoftDelete = 0;
	// 定义时间戳字段名
	protected $createTime = 'create_time';
	protected $updateTime = 'update_time';
	protected $autoWriteTimestamp = true;
	protected $readonly = ['id']; //只读字段不允许修改

}