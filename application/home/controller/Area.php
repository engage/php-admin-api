<?php
namespace app\home\controller;
use app\common\model\Area as Mod;
// use app\home\model\User as UserModel;
use yichenthink\utils\ReturnMsg;

class Area extends Base {
	// 查询列表

	public function list($id = 0, $isTrashed = 0) {
		$message = '没有数据';
		$code = 400;
		$map = [];
		$data = null;
		if (isset($id) && !empty($id) && $id > 99999) {
			//向上查询3级的地区 依次去掉2位补0
			$id = [$id, (int) (substr($id, 0, 4) . '00'), (int) (substr($id, 0, 2) . '0000')];

			$map[] = ['id', 'in', $id];
		}
		$data = Mod::where($map)->select();
		if ($data) {
			$code = 200;
			$message = '成功';
		}
		ReturnMsg::returnMsg($code, $message, $data);
	}
	public function detail($id = 0, $isTrashed = 0) {
		$message = '没有数据';
		$code = 400;
		$map = [];
		$data = null;
		if (isset($id) && !empty($id) && $id > 99999) {
			//查询3级的地区 依次去掉2个00
			$id = [$id, (int) (substr($id, 0, 4) . '00'), (int) (substr($id, 0, 2) . '0000')];

			$map[] = ['id', 'in', $id];
			$data = Mod::where($map)->select();
		}

		if ($data) {
			$code = 200;
			$message = '成功';
		}
		ReturnMsg::returnMsg($code, $message, $data);
	}

}
