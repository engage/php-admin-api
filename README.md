## 作者
~~~
作者：yichen
email：2782268022@qq.com
qq群：302445325
点击链接加入群聊【thinkphp技术交流群】：https://jq.qq.com/?_wv=1027&k=5KycEAO
采用 ThinkPHP 5.1（LTS版本） 
~~~

[功能介绍视频](https://mparticle.uc.cn/video.html?uc_param_str=frdnsnpfvecpntnwprdssskt&wm_aid=49e95f6ca0994074b239cb4deff4cbff)
[接口管理-功能介绍视频观看](https://mparticle.uc.cn/video.html?uc_param_str=frdnsnpfvecpntnwprdssskt&wm_aid=cac7ff66a8d94186a3d8207b33908d5d)
[在线预览web](http://cs.01film.cn/qiye/)
===============
## ThinkPHP5的运行环境要求PHP5.6以上。

## 安装
```
1.下载
1.1 下载composer依赖管理工具,安装请参考https://packagist.org/ 
1.2 更新依赖：项目根目录使用命令 composer  update

2.域名绑定到public目录下

3.入口api接口 
3.1 内容管理后台admin.php
3.2 系统接口后台 system.php
3.3 前台接口 home.php
3.4 初始化安装配置数据库 install.php

4. 配置数据库 打开 你的域名/install.php 
步骤1(必须)，配置数据库名 密码等信息
步骤2（必须），创建数据表
步骤3(可选)，导入数据，这个主要是导入了地区表， 和登陆/注册/验证码 这三个接口免验证可访问。
步骤4（必须），设置后台超级管理员账号+密码。
步骤5 为了保障后台安全 请设置完毕后 删除install.php 或者给改名成不可执行文件，例如install.php.bak
提示:以后如果忘记后台账号密码，还可以用这个文件创建新管理员账号密码。然后登陆后台进行修改或者删除原来的账号。
```
然后就可以在浏览器中访问接口 你的域名/admin.php/login/login 登陆入口。注意这只是api接口。具体使用要配合前端调用。

## 全部源码下载地址
三个分支:可分离部署web站点。也可以部署到一个站点
请根据tag 版本号下载 大版本号对应即可
如1.0.0——1.0.n都可以 

1站.web前端   https://gitee.com/wokaixin/qiye 
2站.api开发管理web端  https://gitee.com/wokaixin/vue-admin
3站.api接口php服务端  https://gitee.com/wokaixin/php-admin-api


## 在线手册

+ [完全开发手册](https://www.kancloud.cn/manual/thinkphp5_1/content)
+ [升级指导](https://www.kancloud.cn/manual/thinkphp5_1/354155) 

## 目录结构

初始的目录结构如下：

~~~
www  WEB部署目录（或者子目录）
├─application           应用目录
│  ├─common             公共模块目录（可以更改）
│  │  ├─model      公共模型目录
│  │  │  │
│  │  │  ├─Model            模型基地 根据模型名,自动匹配自定义的数据库连接
│  │  │  ├─Ad               广告
│  │  │  ├─AdCategory       广告分类
│  │  │  ├─SystemSafe           后台入口安全设置
│  │  │  ├─SystemSafeGrade      后台入口安全设置级别对应名称
│  │  │  ├─SystemSafeUrl        后台入口入口  无需登陆就可以访问的地址
│  │  │  │
│  │  │  ├─Admin                后台开发管理成员
│  │  │  ├─AdminBase            公共基类
│  │  │  ├─AdminFunc            网站api接口功能管理
│  │  │  ├─AdminGroup           开发团队组
│  │  │  ├─AdminGroupFunc       开发团队组关联功能权限
│  │  │  │
│  │  │  ├─AdminRank            后台操作员职务分组                            
│  │  │  ├─AdminRankFunc        后台操作员职务组关联功能                       
│  │  │  ├─AdminUser            后台人员列表                                 
│  │  │  │
│  │  │  ├─Area             全国地区代码
│  │  │  ├─Base             控制器公共基类 后台整体鉴权拦截业务在这里
│  │  │  ├─Nav              导航
│  │  │  ├─News             新闻
│  │  │  ├─NewsCategory     新闻分类
│  │  │  ├─NewsContent      新闻内容
│  │  │  ├─NewsImage        新闻图片列表                                                                                    
│  │  │  ├─User             用户入口
│  │  │  └─ ...            更多工具
│  │  └─ ...            更多
│  ├─system        		系统api管理模块目录 开发管理
│  │  ├─controller      控制器目录
│  │  │  ├─SystemSafe           后台入口安全设置
│  │  │  ├─SystemSafeGrade      后台入口安全设置级别对应名称
│  │  │  ├─SystemSafeUrl        后台入口入口  无需登陆就可以访问的地址
│  │  │  │
│  │  │  ├─Admin      		    后台开发管理成员
│  │  │  ├─AdminBase            公共基类
│  │  │  ├─AdminFunc      	    网站api接口功能管理
│  │  │  ├─AdminGroup           开发团队组
│  │  │  ├─AdminGroupFunc       开发团队组关联功能权限
│  │  │  │
│  │  │  ├─AdminRank            后台操作员职务分组                            考虑废除-移植到内容管理admin模块
│  │  │  ├─AdminRankFunc        后台操作员职务组关联功能                       考虑废除-移植到内容管理admin模块
│  │  │  ├─AdminUser      		后台人员列表                                  考虑废除-移植到内容管理admin模块
│  │  │  │
│  │  │  ├─Base      		控制器公共基类 后台整体鉴权拦截业务在这里
│  │  │  ├─Login      		登陆                                             
│  │  │  ├─User      		用户入口
│  │  │  └─Verify      		验证类验证码等
│  │  ├─model           模型目录
│  │  │  ├─Admin      		    后台管理
│  │  │  ├─AdminBase            后台管理公共基类
│  │  │  ├─AdminFunc      	    后台功能管理
│  │  │  ├─AdminFuncRelation    后台功能和职务关联管理
│  │  │  ├─AdminRank      		后台职务管理
│  │  │  ├─AdminUser      		后台人员团队管理
│  │  │  ├─Area      		地区模型
│  │  │  └─User      		用户模型类
│  │  └─ ...            更多类库目录
│  │ 
│  ├─admin              内容管理模块目录 站长管理
│  │  ├─controller      控制器目录
│  │  │  ├─Ad               广告
│  │  │  ├─AdCategory       广告分类
│  │  │  │
│  │  │  ├─AdminFunc            后台功能管理
│  │  │  ├─AdminRank            后台操作员职务分组                            
│  │  │  ├─AdminRankFunc        后台操作员职务组关联功能                       
│  │  │  ├─AdminUser            后台人员列表                                  
│  │  │  |
│  │  │  ├─Area             全国地区代码
│  │  │  ├─Base             控制器公共基类 后台整体鉴权拦截业务在这里
│  │  │  ├─Nav              导航
│  │  │  ├─News             新闻
│  │  │  ├─NewsCategory     新闻分类
│  │  │  ├─NewsImage        新闻图片列表                                           
│  │  │  ├─User             用户
│  │  │  ├─Login            登陆    
│  │  │  └─*
│  │  ├─model           模型目录
│  │  │  ├─Admin                后台管理
│  │  │  ├─AdminFunc            后台功能管理
│  │  │  ├─AdminFuncRelation    后台功能和职务关联管理
│  │  │  ├─AdminRank            后台职务管理
│  │  │  ├─AdminUser            后台人员团队管理
│  │  │  ├─Area             地区模型
│  │  │  └─User             用户模型类
│  │  └─ ...            更多类库目录
│  │
│  ├─home               前台公共接口
│  │  ├─controller      控制器目录
│  │  │  ├─Ad               广告                             
│  │  │  |
│  │  │  ├─Area             全国地区代码
│  │  │  ├─Base             控制器公共基类 后台整体鉴权拦截业务在这里
│  │  │  ├─Nav              导航
│  │  │  ├─News             新闻
│  │  │  ├─NewsCategory     新闻分类
│  │  │  ├─Login            登录                                           
│  │  │  ├─User             用户
│  │  │  └─*
│  │  ├─model           模型目录
│  │  │  ├─NewsCategory     新闻分类
│  │  │  ├─NewsImage        新闻图片列表       
│  │  │  ├─Area             地区模型
│  │  │  └─User             用户模型类
│  │  └─ ...            更多类库目录
│  │ 
│  ├─command.php        命令行定义文件
│  ├─common.php         公共函数文件
│  └─tags.php           应用行为扩展定义文件
│
├─config                应用配置目录
│  ├─module_name        模块配置目录
│  │  ├─database.php    数据库配置
│  │  ├─cache           缓存配置
│  │  └─ ...            
│  │
│  ├─app.php            应用配置
│  ├─cache.php          缓存配置
│  ├─cookie.php         Cookie配置
│  ├─database.php       数据库配置
│  ├─log.php            日志配置
│  ├─session.php        Session配置
│  ├─template.php       模板引擎配置
│  └─trace.php          Trace配置
│
├─route                 路由定义目录
│  ├─route.php          路由定义
│  └─...                更多
│
├─public                WEB目录（对外访问目录,域名绑定目录）
│  ├─index.php          入口文件 
│  ├─router.php         快速测试文件
│  └─.htaccess          用于apache的重写
│
├─extend                扩展类库目录
├─runtime               应用的运行时目录（可写，可定制）
├─sql                   sql数据库文件目录
│  ├─admin.sql          管理相关基础数据
│  ├─news.sql           内容板块数据
│  └─..
├─vendor                第三方类库目录（Composer依赖库）
│  └──topthink            
│     ├─think-captcha           验证码类
│     └─目录
├─build.php             自动生成定义文件（参考）
├─composer.json         composer 定义文件
├─LICENSE.txt           授权说明文件
├─README.md             README 文件
├─think                 命令行入口文件
~~~
### 应用接口参考
>把URL换成自己的就可以了。下面是目前以及开放的接口地址
>URL=域名.com
前台api入口
var Api = URL+/home.php;
后台api入口
var adminAPI=URL+/admin.php;
后台开发管理api入口
var systemAPI=URL+/system.php;

```
/**
 * api 接口默认值及相关参数
 */
export default {
    // 全部功能名列表
    AdminAdminFunc_List: {
        url: adminAPI + "/admin_func/list",
        params: {
            access_token: true
        } //用户get请求的数据
    },
    // 添加管理人员
    AdminAdminUser_Add: {
        url: adminAPI + "/admin_user/add",
        method:'POST',
        data: {
            access_token: true
        } //用户get请求的数据
    },
    // 管理团队列表
    AdminAdminUser_List:{
        url: adminAPI + "/admin_user/list",
        params: {
            access_token: true
        } //用户get请求的数据
    },
    AdminAdminUser_update: {
        url: adminAPI + "/admin_user/update",
        method: 'PUT',
        data: {
            access_token: true
        }
    },
     // 删除单条
    AdminAdminUser_Delete: {
        url: adminAPI + "/admin_user/delete",
        method: 'DELETE',
        data: {
            user_id: '0', //要恢复的用户id
            access_token: true
        }
    },
    // 恢复一条
    AdminAdminUser_Restore: {
        url: adminAPI + "/admin_user/restore",
        method: 'PUT',
        data: {
            user_id: 0, //要恢复的用户id
            access_token: true
        }
    },
    AdminAdminUser_Myupdate: {
        url: adminAPI + "/admin_user/MyUpdate",
        method: 'PUT',
        data: {
            access_token: true
        }
    },
      // 全部权限功能列表
    AdminAdminRank_List: {
        url: adminAPI + "/admin_rank/list",
        params: {
            access_token: true
        } //用户get请求的数据
    },
    // 用户的功能
    AdminAdminRank_MyRankFunc: {
        url: adminAPI + "/admin_rank/MyRankFunc",
        params: {
            access_token: true
        } //用户get请求的数据
    },
        // 删除单条
    AdminAdminRank_Add: {
        url: adminAPI + "/admin_rank/add",
        method: 'POST',
        params: {
            access_token: true
        }
    }, // 删除单条
    AdminAdminRank_Delete: {
        url: adminAPI + "/admin_rank/delete",
        method: 'DELETE',
        data: {
            user_id: '0', //要恢复的用户id
            access_token: true
        }
    },
    // 恢复一条
    AdminAdminRank_Restore: {
        url: adminAPI + "/admin_rank/restore",
        method: 'PUT',
        data: {
            user_id: 0, //要恢复的用户id
            access_token: true
        }
    },
    // 修改
    AdminAdminRank_Update: {
        url: adminAPI + "/admin_rank/update",
        method: 'PUT',
        data: {
            access_token: true
        },
    },
    
    AdminAdminRankFunc_Update: {
        url: adminAPI + "/admin_rank_func/update",
        method: 'PUT',
        data: {
            access_token: true
        }
    },
    AdminAdminRankFunc_Delete: {
        url: adminAPI + "/admin_rank_func/delete",
        method: 'DELETE',
        data: {
            access_token: true
        }
    },
    // 给用户添加新功能
    AdminAdminRankFunc_Add: {
        url: adminAPI + "/admin_rank_func/add",
        method: 'POST',
        data: {
            access_token: true
        }
    },
    
    
    
    
    
    
    Area_List:{
        url: Api + "/area/list",
    },
    adList:{
        url: Api + "/ad/list",
    },
// 查询多条
   AdminUser_List: {
       url: adminAPI + "/user/list",
       headers: {}, //设置请求头
       data: {}, //用于其他请求的数据
       params: {
           access_token: true,
           p: 0, //页码
           start_time: 15655556666, //起始时间
           last_time: 15655576666, //截至时间
           trashed: 0, //是否被删除的
       } //用户get请求的数据
   },
   // 修改
   AdminUser_update: {
       url: adminAPI + "/user/update",
       method: 'PUT',
       data: {
           access_token: true
       },
       headers: {
           // 'Content-Type':'application/x-www-form-urlencoded;charset=UTF-8'
           // 'Content-Type':'multipart/form-data'
           // "signature":JSON.stringify(configure.signature),
           // "Content-Type":"application/x-www-form-urlencoded"
       }
   },
   // 删除单条
   AdminUser_Delete: {
       url: adminAPI + "/user/delete",
       method: 'DELETE',
       data: {
           id: '0', //要恢复的用户id
           access_token: true
       }
   },
   // 删除多条
   AdminUser_DeleteAll: {
       url:adminAPI + "/user/deleteAll",
       method: 'DELETE',
       data: {
           list: [], //要恢复的用户id
           access_token: true
       }
   },
   // 恢复一条
   AdminUser_Restore: {
       url: adminAPI + "/user/restore",
       method: 'PUT',
       data: {
           id: 0, //要恢复的用户id
           access_token: true
       }
   },
   // 恢复多条
  AdminUser_RestoreAll: {
       url: adminAPI + "/user/restoreAll",
       method: 'PUT',
       data: {
           list: [], //要恢复的用户id
           access_token: true
       }
   },
   // 添加用户
   AdminUser_Add: {
       url: adminAPI + "/user/add",
       method: 'POST',
       data: {
           username: '',
           password: '',
           rank: 0, //权限
           access_token: true
       }
   },
    AdminAd_List:{
        url: adminAPI + "/ad/list",
        params:{
            access_token: true
        }
    },
    AdminAdCategory_List:{
        url: adminAPI + "/ad_category/list",
        params:{
            access_token: true
        }
    },
    AdminAdCategory_Add:{
        url: adminAPI + "/ad_category/add",
        method: 'POST',
        data: {
            access_token: true
        } 
    },
    AdminAdCategory_Update:{
        url: adminAPI + "/ad_category/update",
        method: 'PUT',
        data: {
            access_token: true
        } 
    },
     // 要删除的图片
    UserAdCategory_Delete: {
        url: adminAPI + "/ad_category/delete",
        method: 'DELETE',
        data: {
            id: '0', 
            access_token: true
        }
    },
    AdminAd_Detail:{
       url: adminAPI + "/ad/detail",
       params:{
           access_token: true
       }
    },
    AdminAd_Add:{
       url: adminAPI + "/ad/add",
       method: 'POST',
       data: {
           access_token: true
       } 
    },
   AdminAd_Update:{
       url: adminAPI + "/ad/update",
       method: 'PUT',
       data: {
           access_token: true
       } 
    },
    AdminAd_Delete: {
        url: adminAPI + "/ad/delete",
        method: 'DELETE',
        data: {
            id: '0', //要恢复的用户id
            access_token: true
        }
    },AdminAd_Restore: {
        url: adminAPI + "/ad/restore",
        method: 'PUT',
        data: {
            id: 0, //要恢复的用户id
            access_token: true
        }
    },
    NavList:{
        url: Api + "/nav/list",
    },
    newsDetail:{
       url: Api + "/news/detail",
       params:{
          id:'' 
       }
    },
    NewsCategory_List:{
        url: Api + "/news_category/list",
    },

    newsList:{
       url: Api + "/news/list",
       params:{
           
       }
    },
    AdminNav_List:{
       url: adminAPI + "/nav/list", 
       params:{
           access_token: true
       }
    },
    AdminNav_Add:{
        url: adminAPI + "/nav/add",
        method: 'POST',
        data: {
            access_token: true
        } 
    },
    AdminNav_Update:{
        url: adminAPI + "/nav/update",
        method: 'PUT',
        data: {
            access_token: true
        } 
    },
         // 要删除的图片
    AdminNav_Delete: {
        url: adminAPI + "/nav/delete",
        method: 'DELETE',
        data: {
            id: '0', 
            access_token: true
        }
    },
    AdminNewsCategory_List:{
        url: adminAPI + "/news_category/list",
        params:{
            access_token: true
        }
    },
    AdminNewsCategory_Add:{
        url: adminAPI + "/news_category/add",
        method: 'POST',
        data: {
            access_token: true
        } 
    },
    AdminNewsCategory_Update:{
        url: adminAPI + "/news_category/update",
        method: 'PUT',
        data: {
            access_token: true
        } 
    },
     // 要删除的图片
    AdminNewsCategory_Delete: {
        url: adminAPI + "/news_category/delete",
        method: 'DELETE',
        data: {
            id: '0', 
            access_token: true
        }
    },
    AdminNews_Add:{
       url: adminAPI + "/news/add",
       method: 'POST',
       data: {
           access_token: true
       } 
    },
    AdminNews_Update:{
       url: adminAPI + "/news/update",
       method: 'PUT',
       data: {
           access_token: true
       } 
    },
    AdminNews_List:{
       url: adminAPI + "/news/list",
       params: {
           access_token: true
       } 
    },
    AdminNews_Detail:{
       url: adminAPI + "/news/detail",
       params: {
           access_token: true
       } 
    },
    AdminNews_Delete: {
        url: adminAPI + "/news/delete",
        method: 'DELETE',
        data: {
            id: '0', //要恢复的用户id
            access_token: true
        }
    },
    AdminNews_Restore: {
        url: adminAPI + "/news/restore",
        method: 'PUT',
        data: {
            id: 0, //要恢复的用户id
            access_token: true
        }
    },
    AdminNewsImage_DetailedList:{
        url: adminAPI + "/news_image/detailedList",
        params: {
            access_token: true
        } 
    },
    AdminNewsImage_Upload:{
        url: adminAPI + "/news_image/upload",
        method: 'POST',
        data: {
            access_token: true
        } 
    },
    // 要删除的图片
    AdminNewsImage_Delete: {
        url: adminAPI + "/news_image/delete",
        method: 'DELETE',
        data: {
            id: '0', 
            access_token: true
        }
    },


    // 登陆
    Login: {
        url: Api + "/login/login",
        method: 'GET',
        params: {
            username: '',
            password: '',
            captcha: '', //验证码
            access_token: true
        }
    },
    resetLogin: {
        url: Api + "/login/resetLogin",
        method: 'PUT',
        params: {
            access_token: true
        }
    },
    // 注册
    Register: {
        url:  Api + "/login/register",
        method: 'POST',
        data: {
            username: '',
            password: '',
            captcha: '', //验证码
            access_token: true
        }
    },
    Captcha: {
        url:  Api + "/verify/captcha",
        method: 'GET',
        params: {
            id: 'captcha', //验证场景
            access_token: true
        }
    }
}


```
###开发人员接口管理api
```
/**
 * api 接口默认值及相关参数
 */
export default {
    // 查询多条
    Admin_List: {
        url: systemAPI + "/admin/list",
        headers: {}, //设置请求头
        data: {}, //用于其他请求的数据
        params: {
            access_token: true,
            p: 0, //页码
            start_time: 15655556666, //起始时间
            last_time: 15655576666, //截至时间
            trashed: 0, //是否被删除的
        } //用户get请求的数据
    },
    // 恢复一条
    Admin_Restore: {
        url: systemAPI + "/admin/restore",
        method: 'PUT',
        data: {
            user_id: 0, //要恢复的用户id
            access_token: true
        }
    }, // 删除单条
    Admin_Delete: {
        url: systemAPI + "/admin/delete",
        method: 'DELETE',
        data: {
            user_id: '0', //要恢复的用户id
            access_token: true
        }
    }, // 添加后台管理人员
    Admin_Add: {
        url: systemAPI + "/admin/add",
        method: 'POST',
        data: {
            user_id: '',
            rank: 0, //权限
            access_token: true
        }
    },
    Admin_Update: {
        url: systemAPI + "/admin/update",
        method: 'PUT',
        data: {
            access_token: true
        }
    },
    SystemSafeGrade_List: {
        url: systemAPI + "/system_safe_grade/list",
        params: {
            access_token: true
        } //用户get请求的数据 
    },
    SystemSafe_Detail: {
        url: systemAPI + "/system_safe/detail",
        params: {
            access_token: true
        } //用户get请求的数据 
    },
    SystemSafe_Update: {
        url: systemAPI + "/system_safe/update",
        method: 'PUT',
        data: {
            access_token: true
        }
    },
    SystemSafeUrl_List: {
        url: systemAPI + "/system_safe_url/list",
        params: {
            access_token: true
        } //用户get请求的数据 
    },
    SystemSafeUrl_Update: {
        url: systemAPI + "/system_safe_url/update",
        method: 'PUT',
        data: {
            access_token: true
        }
    },
    SystemSafeUrl_Add: {
        url: systemAPI + "/system_safe_url/add",
        method: 'POST',
        data: {
            access_token: true
        }
    },
    // 删除一条
    SystemSafeUrl_Delete: {
        url: systemAPI + "/system_safe_url/delete",
        method: 'DELETE',
        data: {
            user_id: '0', //要恢复的用户id
            access_token: true
        }
    },
    // 恢复一条
    SystemSafeUrl_Restore: {
        url: systemAPI + "/system_safe_url/restore",
        method: 'PUT',
        data: {
            user_id: 0, //要恢复的用户id
            access_token: true
        }
    },
    AdminUser_List: {
        url: systemAPI + "/admin_user/list",
        params: {
            access_token: true
        } //用户get请求的数据 
    },
    AdminUser_update: {
        url: systemAPI + "/admin_user/update",
        method: 'PUT',
        data: {
            access_token: true
        }
    },
    // 添加后台管理人员
    AdminUser_Add: {
        url: systemAPI + "/admin_user/add",
        method: 'POST',
        data: {
            user_id: '',
            rank: 0, //权限
            access_token: true
        }
    }, // 删除单条
    AdminUser_Delete: {
        url: systemAPI + "/admin_user/delete",
        method: 'DELETE',
        data: {
            user_id: '0', //要恢复的用户id
            access_token: true
        }
    },
    // 恢复一条
    AdminUser_Restore: {
        url: systemAPI + "/admin_user/restore",
        method: 'PUT',
        data: {
            user_id: 0, //要恢复的用户id
            access_token: true
        }
    },
    AdminUser_Myupdate: {
        url: systemAPI + "/admin_user/MyUpdate",
        method: 'PUT',
        data: {
            access_token: true
        }
    },
    // 添加一个功能接口
    AdminFunc_Add: {
        url: systemAPI + "/admin_func/add",
        method: 'POST',
        data: {
            access_token: true
        }
    },
    // 添加一个功能接口
    AdminFunc_Update: {
        url: systemAPI + "/admin_func/update",
        method: 'PUT',
        data: {
            access_token: true
        }
    },
    // 全部功能名列表
    AdminFunc_List: {
        url: systemAPI + "/admin_func/list",
        params: {
            access_token: true
        } //用户get请求的数据
    },
    //全部功能详版 用于功能新增修改管理
    AdminFunc_DetailedList: {
        url: systemAPI + "/admin_func/detailedList",
        params: {
            access_token: true
        }
    },
    // 删除单条
    AdminFunc_Delete: {
        url: systemAPI + "/admin_func/delete",
        method: 'DELETE',
        data: {
            id: '0', //要恢复的用户id
            access_token: true
        }
    },
    // 恢复一条
    AdminFunc_Restore: {
        url: systemAPI + "/admin_func/restore",
        method: 'PUT',
        data: {
            id: 0, //要恢复的用户id
            access_token: true
        }
    },
    // 全部权限功能列表
    AdminRank_List: {
        url: systemAPI + "/admin_rank/list",
        params: {
            access_token: true
        } //用户get请求的数据
    },
    // 用户的功能
    AdminRank_MyRankFunc: {
        url: systemAPI + "/admin_rank/MyRankFunc",
        params: {
            access_token: true
        } //用户get请求的数据
    },
        // 删除单条
    AdminRank_Add: {
        url: systemAPI + "/admin_rank/add",
        method: 'POST',
        params: {
            access_token: true
        }
    }, // 删除单条
    AdminRank_Delete: {
        url: systemAPI + "/admin_rank/delete",
        method: 'DELETE',
        data: {
            user_id: '0', //要恢复的用户id
            access_token: true
        }
    },
    // 恢复一条
    AdminRank_Restore: {
        url: systemAPI + "/admin_rank/restore",
        method: 'PUT',
        data: {
            user_id: 0, //要恢复的用户id
            access_token: true
        }
    },
    // 修改
    AdminRank_Update: {
        url: systemAPI + "/admin_rank/update",
        method: 'PUT',
        data: {
            access_token: true
        },
    },
    
    AdminRankFunc_Update: {
        url: systemAPI + "/admin_rank_func/update",
        method: 'PUT',
        data: {
            access_token: true
        }
    },
    AdminRankFunc_Delete: {
        url: systemAPI + "/admin_rank_func/delete",
        method: 'DELETE',
        data: {
            access_token: true
        }
    },
    // 给用户添加新功能
    AdminRankFunc_Add: {
        url: systemAPI + "/admin_rank_func/add",
        method: 'POST',
        data: {
            access_token: true
        }
    },
    // 给组添加新功能
    AdminGroupFunc_Add: {
        url: systemAPI + "/admin_group_func/add",
        method: 'POST',
        data: {
            access_token: true
        }
    },
    AdminGroupFunc_Delete: {
        url: systemAPI + "/admin_group_func/delete",
        method: 'DELETE',
        data: {
            access_token: true
        }
    },
    AdminGroup_funcList: {
        url: systemAPI + "/admin_group/funcList",
        params: {
            access_token: true
        } //用户get请求的数据
    },
    AdminGroup_List: {
        url: systemAPI + "/admin_group/list",
        params: {
            access_token: true
        } //用户get请求的数据
    },
    AdminGroup_Add: {
        url: systemAPI + "/admin_group/add",
        method: 'POST',
        params: {
            access_token: true
        } //用户get请求的数据
    },
    // 修改
    AdminGroup_Update: {
        url: systemAPI + "/admin_group/update",
        method: 'PUT',
        data: {
            access_token: true
        },
    },
    // 删除单条
    AdminGroup_Delete: {
        url: systemAPI + "/admin_group/delete",
        method: 'DELETE',
        data: {
            user_id: '0', //要恢复的用户id
            access_token: true
        }
    }, // 恢复一条
    AdminGroup_Restore: {
        url: systemAPI + "/admin_group/restore",
        method: 'PUT',
        data: {
            user_id: 0, //要恢复的用户id
            access_token: true
        }
    },
    
 // 查询多条
    UserList: {
        url: systemAPI + "/user/list",
        headers: {}, //设置请求头
        data: {}, //用于其他请求的数据
        params: {
            access_token: true,
            p: 0, //页码
            start_time: 15655556666, //起始时间
            last_time: 15655576666, //截至时间
            trashed: 0, //是否被删除的
        } //用户get请求的数据
    },
    // 修改
    User_update: {
        url: systemAPI + "/user/update",
        method: 'PUT',
        data: {
            access_token: true
        },
        headers: {
            // 'Content-Type':'application/x-www-form-urlencoded;charset=UTF-8'
            // 'Content-Type':'multipart/form-data'
            // "signature":JSON.stringify(configure.signature),
            // "Content-Type":"application/x-www-form-urlencoded"
        }
    },
    // 删除单条
    User_Delete: {
        url: systemAPI + "/user/delete",
        method: 'DELETE',
        data: {
            id: '0', //要恢复的用户id
            access_token: true
        }
    },
    // 删除多条
    User_DeleteAll: {
        url: systemAPI + "/user/deleteAll",
        method: 'DELETE',
        data: {
            list: [], //要恢复的用户id
            access_token: true
        }
    },
    // 恢复一条
    User_Restore: {
        url: systemAPI + "/user/restore",
        method: 'PUT',
        data: {
            id: 0, //要恢复的用户id
            access_token: true
        }
    },
    // 恢复多条
    User_RestoreAll: {
        url: systemAPI + "/user/restoreAll",
        method: 'PUT',
        data: {
            list: [], //要恢复的用户id
            access_token: true
        }
    },
    // 添加用户
    User_Add: {
        url: systemAPI + "/user/add",
        method: 'POST',
        data: {
            username: '',
            password: '',
            rank: 0, //权限
            access_token: true
        }
    },
    // 登陆
    Login: {
        url:API + "/login/login",
        method: 'GET',
        params: {
            username: '',
            password: '',
            captcha: '', //验证码
            access_token: true
        }
    },
    resetLogin: {
        url: API + "/login/resetLogin",
        method: 'PUT',
        params: {
            access_token: true
        }
    },
    // 注册
    Register: {
        url: API + "/login/register",
        method: 'POST',
        data: {
            username: '',
            password: '',
            captcha: '', //验证码
            access_token: true
        }
    },
    Captcha: {
        url: API + "/verify/captcha",
        method: 'GET',
        params: {
            id: 'captcha', //验证场景
            access_token: true
        }
    }
}
```
> 可以使用php自带webserver快速测试
> 切换到根目录后，启动命令：php think run

## 命名规范 
操作thinkphp5.1命名规范
## 参与开发

请参阅 [ThinkPHP5 核心框架包](https://github.com/top-think/framework)。

## 版权信息

ThinkPHP遵循Apache2开源协议发布，并提供免费使用。

本项目包含的第三方源码和二进制文件之版权信息另行标注。
